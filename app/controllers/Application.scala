package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Form
import play.api.data.Forms._
import models.Comment
import views._
import math._

object Application extends Controller {
  
  val commentForm = Form(
    "comment" -> nonEmptyText
  )
  var aaa = ""
  
    
  def index = Action {
    Ok(views.html.index(Comment.all()))
  }

  def postComment(comment: String) = Action { implicit request =>
    Comment.insertDB(comment)
    Ok
  }
  
  def postDelete(id: Long) = Action { implicit request =>
    Comment.deleteDB(id)
    Ok
  }

  def javascriptRoutes = Action { implicit request =>
    import routes.javascript._
    Ok(
       Routes.javascriptRouter("jsRoutes", Some("jQuery.ajax"))(
           routes.javascript.Application.index,
           routes.javascript.Application.postComment,
           routes.javascript.Application.postDelete
           )
    ).as("text/javascript")
  }
}