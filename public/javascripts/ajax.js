 $('#comment-form').submit(function(event) {
			event.preventDefault();
			var comment = $("#input-comment").val();
			console.log(comment);

  			jsRoutes.controllers.Application.postComment(comment).ajax({
  				
    			// 送信したらボタンを使用不可にし二重投稿を防ぐ
    			beforeSend: function() {
      				$("#comment-submit").attr('disabled', true);
    			},
    			// 完了したらボタン使用不可を解除する
    			complete: function() {
      				$("#comment-submit").attr('disabled', false);
    			},
    			// 投稿が成功したら新しいコメントを表示する
    			success: function(datetime) {
      				$("#input-comment").val("");
      				$('#comments').append("<li>" + comment + "</li>");
    			},
    			error: function() {
      				alert("error");
    			}
  			})
		});