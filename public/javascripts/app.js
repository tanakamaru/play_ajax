    // ngResource を依存モジュールに指定
    angular.module("app", ["ngResource"]);

    // メインコントローラー
    function MainCtrl($scope, $resource) {
      // API の URL を指定してモデルを作成
      var Guest = $resource("/guests");

      // query でコレクションを取得
      $scope.guests = Guest.query();

      $scope.guestName = "";

      $scope.addGuest = function() {
        Guest.save({ name: $scope.guestName }, function(guest) {
          // 作成に成功したら一覧に追加する
          $scope.guests.push(guest);
        });

        $scope.guestName = "";
      };
    }